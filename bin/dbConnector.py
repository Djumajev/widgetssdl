import mysql.connector
from mysql.connector import Error
import json

 
def connect():
    try:
        conn = mysql.connector.connect(host='localhost',
                                       database='test',
                                       user='root',
                                       password='Dbnzgblh321')
        if conn.is_connected():
            print('Connected to MySQL database')
            cursor = conn.cursor()
            cursor.execute("SELECT JSON FROM actual_table")

            rows = cursor.fetchall()
            print('Total Row(s):', cursor.rowcount)
            
            str = '{\n"array": [\n'
            counter = 0
            for row in rows:
                counter += 1
                str += ''.join(row)
                if counter < cursor.rowcount:
                    str += ',\n'
                else:
                    str += '\n'
            str += ']\n}'

            outfile = open('data.json', 'w')
            outfile.write(str)
 
    except Error as e:
        print(e)
 
    finally:
        print('Closed')
        conn.close()
        cursor.close()
 
 
if __name__ == '__main__':
    connect()