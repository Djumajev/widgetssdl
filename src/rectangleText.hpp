#pragma once
#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include <string>

class rectangleText{
public:
    rectangleText(int x, int y, int width, int height, TTF_Font* font);
    ~rectangleText();
    void setBackgroundColor(Uint8 r, Uint8 g, Uint8 b);
    void setTextColor(Uint8 r, Uint8 g, Uint8 b);
    void draw(SDL_Renderer* renderer, int topSize);
    void setSize(int width, int height);
    void setPosition(int x, int y);
    void setMessage(std::string message);
    void setFont(TTF_Font* font, unsigned int fontSize);

private:
    SDL_Color backgroundColor = {0, 0, 0};
    SDL_Color textColor = {0, 0, 0};
    int x, y, width, height;
    std::string message;
    TTF_Font* font = NULL;
    SDL_Surface* surfaceMessage = NULL;
    SDL_Texture* MessageTex = NULL;
    SDL_Rect Message_rect;
    unsigned int fontSize;
};