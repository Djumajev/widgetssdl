#pragma once
#include "include/rapidjson/document.h"
#include <fstream>
#include <string>
#include "taskConstruck.hpp"

using namespace rapidjson;

class JSONParser{
public:
    JSONParser();
    JSONParser(const char* path);
    ~JSONParser();

    void loadFile(const char* path);
    std::string getStringElement(const char* key);
    int getIntElement(const char* key);
    std::vector<taskConstruct> loadTasks();

private:
    void createJSONObject();
    std::string jsonString;
    Document object;
};