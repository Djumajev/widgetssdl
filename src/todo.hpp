#pragma once
#include <SDL2/SDL.h>
#include "window.cpp"
#include "small.cpp"
#include "timer.cpp"
#include <vector>
#include <ctime>
#include <iostream>
#include <thread>   
#include <mutex>
#include "taskConstruck.hpp"

class todo{
public:
    todo();
    ~todo();
    void addTask(taskConstruct task);
    void deleteTask(size_t index);
    void start();

private:
    void addGrid();
    std::vector<small*> windows;
    bool isRunning = true;
    void draw();
    void taskController();
    void makeBigger(size_t index);
    void makeSmaller(size_t index);
    void initTasks();
    timer Timer;

    bool FocusedOne = false;

    //Position
    std::vector<std::pair<int, int>> grid;
    std::vector<taskConstruct> Tasks;
    int maxElementInRow = 10,
        miniFontSize = 100,
        bigFontSize = 50,
        elementWidth = 100,
        elementHeight = 100,
        offset = 20,
        startX = 100,
        startY = 50,
        zoom = 5;

    unsigned int delay = 30;

};