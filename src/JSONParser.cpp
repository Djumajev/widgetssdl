#pragma once
#include "JSONParser.hpp"
#include "include/rapidjson/document.h"
#include <fstream>
#include <string>
#include "taskConstruck.hpp"

using namespace rapidjson;

JSONParser::JSONParser(){

}

JSONParser::JSONParser(const char* path){
    this->loadFile(path);
}

JSONParser::~JSONParser(){

}

void JSONParser::loadFile(const char* path){
    std::ifstream input;
    input.open(path);

    if(!input) return;

    std::string help;
    while(input >> help)
        this->jsonString += help;

    input.close();

    this->createJSONObject();
}

void JSONParser::createJSONObject(){
    this->object.Parse(this->jsonString.c_str());
}

int JSONParser::getIntElement(const char* key){
    return this->object[key].GetInt();
}

std::string JSONParser::getStringElement(const char* key){
    return this->object[key].GetString();
}

std::vector<taskConstruct> JSONParser::loadTasks(){
    const Value& taskConfs = this->object["array"];

    std::vector<taskConstruct> output;

    assert(taskConfs.IsArray()); 
    for (Value::ConstValueIterator itr = taskConfs.Begin(); itr != taskConfs.End(); ++itr) {
        const Value& taskConf = *itr;
        assert(taskConf.IsObject());

        taskConstruct temp = {
            taskConf["bigMessage"].GetString(),
            taskConf["smallMessage"].GetString(),
            taskConf["name"].GetString(),
            {taskConf["textColor"][0].GetInt(), taskConf["textColor"][1].GetInt(), taskConf["textColor"][2].GetInt()},
            {taskConf["topBackground"][0].GetInt(), taskConf["topBackground"][1].GetInt(), taskConf["topBackground"][2].GetInt()},
            {taskConf["background"][0].GetInt(), taskConf["background"][1].GetInt(), taskConf["background"][2].GetInt()}
        };
        output.push_back(temp);
    }

    return output;
}