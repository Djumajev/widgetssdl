#pragma once
#include <SDL2/SDL.h>
#include "rectangle.hpp"

rectangle::rectangle(int x, int y, int width, int height){
    this->x = x;
    this->y = y;
    this->width = width;
    this->height = height;
}

rectangle::~rectangle(){

}

void rectangle::setBackgroundColor(Uint8 r, Uint8 g, Uint8 b){
    this->backgroundColor = {r, g, b, 255};
}

void rectangle::draw(SDL_Renderer* renderer){
    SDL_Rect fillRect;
    fillRect.x = this->x;
    fillRect.y = this->y;
    fillRect.w = this->width;
    fillRect.h = this->height;
     
    SDL_SetRenderDrawColor(renderer, this->backgroundColor.r, this->backgroundColor.g, this->backgroundColor.b, 255); 
    SDL_RenderFillRect(renderer, &fillRect );
}

void rectangle::setSize(int width, int height){
    this->width = width;
    this->height = height;
}
    
void rectangle::setPosition(int x, int y){
    this->x = x;
    this->y = y;
}