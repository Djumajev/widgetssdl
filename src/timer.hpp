#pragma once
#include <ctime>
#include <thread>

class timer{
public:
    timer();
    ~timer();
    void start();
    time_t getTime();
    void loop(int t);
    
private:
    time_t Time;
    void timeCounter();
    bool started = false;
};