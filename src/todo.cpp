#pragma once
#include "todo.hpp"
#include "JSONParser.cpp"
#include "taskConstruck.hpp"
#include <fstream>

todo::todo(){
    this->Timer.start();
    SDL_Init(SDL_INIT_EVERYTHING);

    this->grid.push_back(std::make_pair(this->startX, this->startY));
    //Применить настройки
    JSONParser parser("conf.json");
    this->maxElementInRow = parser.getIntElement("maxElementInRow");
    this->miniFontSize = parser.getIntElement("miniFontSize");
    this->bigFontSize = parser.getIntElement("bigFontSize");
    this->elementWidth = parser.getIntElement("elementWidth");
    this->elementHeight = parser.getIntElement("elementHeight");
    this->offset = parser.getIntElement("offset");
    this->startX = parser.getIntElement("startX");
    this->startY = parser.getIntElement("startY");
    this->zoom = parser.getIntElement("zoom");

    system("python3 dbConnector2.py");

    JSONParser taskParser("data.json");
    std::vector<taskConstruct> tempTask = taskParser.loadTasks();
    for(auto x : tempTask)
        this->addTask(x);
}

todo::~todo(){
    //Удалить весь мусор
    for(unsigned int i = 0; i < this->windows.size(); i++)
        delete this->windows[i];

    this->windows.clear();
    SDL_Quit();
}

void todo::draw(){
    this->initTasks();

    while(this->isRunning){
        for(auto x : this->windows)
            x->draw();

        SDL_Event event;
        while(SDL_PollEvent(&event)){
            switch(event.type){
                case SDL_KEYDOWN:
                    if(event.key.keysym.sym == SDLK_ESCAPE)
                        this->isRunning = false;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    for(size_t i = 0; i < this->windows.size(); i++)
                        if(event.window.windowID == this->windows[i]->getWindowID())
                            if(!this->FocusedOne)
                                this->makeBigger(i);
                            else this->makeSmaller(i);
                break;
                case SDL_WINDOWEVENT:
                    switch(event.window.event){
                        case SDL_WINDOWEVENT_CLOSE:
                            this->isRunning = false;
                            break;
                    }
                break;
            }
        }

        SDL_Delay(this->delay);
        this->taskController();
    }
}

void todo::addTask(taskConstruct task){
    this->windows.push_back(new small(this->elementWidth, this->elementHeight, this->grid.back().first, this->grid.back().second, task.name));
    this->windows.back()->setFontSize(this->miniFontSize);
    this->addGrid();
    this->Tasks.push_back(task);
 }

void todo::taskController(){
    if(this->Timer.getTime() % 30 == 0){
        system("python3 dbConnector2.py");
        //Load updated !!!!!if new file size != old file size
        static unsigned long int lastFileSize = 0;
        std::ifstream in("data.json", std::ifstream::ate | std::ifstream::binary);
        if(lastFileSize != in.tellg()){
            lastFileSize = in.tellg();
            std::cout << "Updated" << std::endl;
            std::cout << lastFileSize << std::endl;
            //Delete all tasks
            
            for(auto x : this->windows)
                delete x;
            this->windows.clear();
            this->grid.clear();
            this->Tasks.clear();
            //Set Starting X and Y
            this->grid.push_back(std::make_pair(this->startX, this->startY));
            
            JSONParser taskParser("data.json");
            std::vector<taskConstruct> tempTask = taskParser.loadTasks();
            for(auto x : tempTask)
                this->addTask(x);

            this->initTasks();
            this->Timer.loop(2);
        }
        in.close();
    }

}

void todo::start(){
    this->draw();
}

void todo::makeBigger(size_t index){
    std::string message = this->Tasks[index].bigMessage;

    //this->windows[index]->setWindowSize(message.size() * this->bigFontSize / this->zoom, message.size() * this->bigFontSize / this->zoom);
    this->windows[index]->setWindowSize(this->elementWidth * this->zoom, this->elementHeight * this->zoom);

    this->windows[index]->setFontSize(this->bigFontSize);
    this->windows[index]->setMessage(message);

    this->windows[index]->setWindowPosition(this->grid[0].first, this->grid[0].second);

    this->FocusedOne = true;

    for(size_t i = 0; i < this->windows.size(); i++)
        if(i == index)
            continue;
        else{
            SDL_HideWindow(this->windows[i]->getWindow());
        }
}

void todo::makeSmaller(size_t index){
    this->windows[index]->setWindowSize(this->elementWidth, this->elementHeight);

    this->windows[index]->setFontSize(this->miniFontSize);
    this->windows[index]->setMessage(this->Tasks[index].smallMessage);

    this->windows[index]->setWindowPosition(this->grid[index].first, this->grid[index].second);


    this->FocusedOne = false;

    for(size_t i = 0; i < this->windows.size(); i++)
        if(i == index)
            continue;
        else{
            SDL_ShowWindow(this->windows[i]->getWindow());
        }
}

void todo::deleteTask(size_t index){
    std::vector<taskConstruct> temp;

    //Copy everything exept this->windows[index]
    for(size_t i = 0; i < this->windows.size(); i++)
        if(i != index)
            temp.push_back(this->Tasks[i]);

    //Clear windows
    for(auto x : this->windows)
        delete x;
    this->windows.clear();

    //Clear grid
    this->grid.clear();

    //Clear tasks
    this->Tasks.clear();
    
    //Set Starting X and Y
    this->grid.push_back(std::make_pair(this->startX, this->startY));

    //add new tasks and grid without index element
    for(auto x : temp)
        this->addTask(x);

    this->initTasks();
}

void todo::addGrid(){
    if(this->grid.size() % this->maxElementInRow == 0)
        this->grid.push_back(std::make_pair(this->startX, this->grid.back().second + this->elementHeight + this->offset));
    else this->grid.push_back(std::make_pair(this->grid.back().first + this->elementWidth + this->offset, this->grid.back().second));
}

void todo::initTasks(){
    for(size_t i = 0; i < this->Tasks.size(); i++){
        this->windows[i]->setBackgroundColor(this->Tasks[i].background.red, this->Tasks[i].background.green, this->Tasks[i].background.blue);
        this->windows[i]->top->setBackgroundColor(this->Tasks[i].topBackground.red, this->Tasks[i].topBackground.green, this->Tasks[i].topBackground.blue);
        this->windows[i]->setTextColor(this->Tasks[i].textColor.red, this->Tasks[i].textColor.green, this->Tasks[i].textColor.blue);
        this->windows[i]->setMessage(this->Tasks[i].smallMessage);
    }
}