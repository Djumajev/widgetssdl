#pragma once
#include "timer.hpp"
#include <ctime>
#include <thread>
#include <SDL2/SDL.h>

timer::timer(){
}

timer::~timer(){

}

void timer::start(){
    this->started = true;
	std::thread newth(&timer::timeCounter, this);
	newth.detach();
}

void timer::timeCounter(){
    while (this->started) {
		time_t new_time = time(0);
		SDL_Delay(1);
		this->Time += time(0) - new_time;
	}
}

time_t timer::getTime(){
    return this->Time;
}

void timer::loop(int t){
	this->Time = t;
}