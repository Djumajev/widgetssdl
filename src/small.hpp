#pragma once
#include "window.hpp"
#include <SDL_ttf.h>
#include "rectangle.cpp"
#include "rectangleText.cpp"

class small : protected window{
public:
    small(int width, int height, int x, int y, std::string name);
    ~small();

    void draw();
    void setMessage(std::string m);
    void setTextColor(Uint8 r, Uint8 g, Uint8 b);
    bool initFonts();
    void setFontSize(unsigned int size);

    using window::setBackgroundColor;
    using window::getX;
    using window::getY;
    using window::setWindowPosition;
    using window::setWindowSize;
    using window::getWindowID;
    using window::getWindow;

    rectangle* top;
    rectangleText* text;
    int TopSizeProcents = 20;
    std::string name;

private:
    std::string message;
    int oldWidth, oldHeight;
    int topSize, fontSize;

protected:
    TTF_Font* font = NULL;
    SDL_Color textColor = {0, 0, 0}; 

};