#pragma once
#include "small.hpp"
#include <iostream>
#include <SDL_ttf.h>
#include "rectangle.cpp"
#include "rectangleText.cpp"


small::small(int width, int height, int x, int y, std::string name) : window(width, height, x, y, name){
    this->name = name;
    this->initFonts();
    this->topSize = (this->height * this->TopSizeProcents) / 100;
    this->top = new rectangle(0, 0, this->width, this->topSize);
    this->text = new rectangleText(0, 20, this->width, this->height, this->font);

    this->oldWidth = width;
    this->oldHeight = height;
}

small::~small(){
    delete this->top;
    delete this->text;
    SDL_DestroyRenderer(this->renderer);
    this->~window();
    TTF_CloseFont(this->font);
    TTF_Quit();
}

void small::draw(){
    SDL_SetRenderDrawColor(this->renderer, this->backgroundColor.r, this->backgroundColor.g, this->backgroundColor.b, 255);
    SDL_RenderClear(this->renderer);

    this->text->setMessage(this->message);
    
    if(this->oldWidth != this->width || this->oldHeight != this->height){
        this->oldWidth = this->width;
        this->oldHeight = this->height;
        this->topSize = (this->height * this->TopSizeProcents) / 100;

        this->text->setSize(this->width, this->height);
        this->top->setSize(this->width, this->topSize);
    }

    this->text->draw(this->renderer, this->topSize);
    this->top->draw(this->renderer);


    SDL_RenderPresent(this->renderer);
}

void small::setMessage(std::string m){
    if(this->message != m)
        this->message = m;
}

void small::setTextColor(Uint8 r, Uint8 g, Uint8 b){
    this->text->setTextColor(r, g, b);
}

bool small::initFonts(){
    if (TTF_Init() < 0)
        return false;  

    //Font size

    this->font = TTF_OpenFont("ttf/FreeMonoOblique.ttf", this->fontSize);
    if(!font)
        return false;

    return true;

}

void small::setFontSize(unsigned int size){
    if(this->fontSize != size){
        this->fontSize = size;
        this->font = TTF_OpenFont("ttf/FreeMonoOblique.ttf", this->fontSize);
        this->text->setFont(this->font, size);
    }
  
}