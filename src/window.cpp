#pragma once
#include "window.hpp"
#include <iostream>
#include <SDL2/SDL.h>

window::window(int width, int height, int x, int y, std::string name){
    this->width = width;
    this->height = height;
    this->x = x;
    this->y = y;
    this->name = name;

    this->createWindow();
}

window::~window(){
    SDL_DestroyWindow(this->myWindow);
    SDL_DestroyRenderer(this->renderer);
}

void window::createWindow(){
    Uint32 flags = SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS;
    this->myWindow = SDL_CreateWindow
    (
        this->name.c_str(),
        this->x,
        this->y,
        this->width,
        this->height,
        flags
    );
   
    this->renderer = SDL_CreateRenderer( this->myWindow, -1, SDL_RENDERER_ACCELERATED | 0);
    SDL_SetRenderDrawBlendMode(this->renderer, SDL_BLENDMODE_NONE);
}

void window::draw(){}

Uint8 window::getWindowID(){
    return SDL_GetWindowID(this->myWindow);
}

void window::setWindowSize(int width, int height){
    this->width = width;
    this->height = height;
    SDL_SetWindowSize(this->myWindow, this->width, this->height);
}

void window::setWindowPosition(int x, int y){
    this->x = x;
    this->y = y;
    SDL_SetWindowPosition(this->myWindow, this->x, this->y);
}

int window::getX(){
    return this->x;
}

int window::getY(){
    return this->y;
}

void window::setBackgroundColor(Uint8 r, Uint8 g, Uint8 b){
    this->backgroundColor = {r ,g ,b};
}

SDL_Window* window::getWindow(){
    return this->myWindow;
}