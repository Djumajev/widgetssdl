#pragma once
#include <string>
#include <SDL2/SDL.h>

class window{
public:
    window(int width, int height, int x, int y, std::string name);
    ~window();
    virtual void draw();
    Uint8 getWindowID();
    void setWindowSize(int width, int height);
    void setWindowPosition(int x, int y);
    void setBackgroundColor(Uint8 r, Uint8 g, Uint8 b);
    SDL_Window* getWindow();
    int getX();
    int getY();

private:
    std::string name;
    void createWindow();

protected:
    int width, height, x ,y;
    SDL_Window* myWindow = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_Color backgroundColor = {0, 0, 0};
};