#pragma once
#include <SDL2/SDL.h>

class rectangle{
public:
    rectangle(int x, int y, int width, int height);
    ~rectangle();
    void setBackgroundColor(Uint8 r, Uint8 g, Uint8 b);
    void draw(SDL_Renderer* renderer);
    void setSize(int width, int height);
    void setPosition(int x, int y);

private:
    SDL_Color backgroundColor = {0, 0, 0};
    int x, y, width, height;
};