#pragma once
#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include "rectangleText.hpp"
#include <string>

rectangleText::rectangleText(int x, int y, int width, int height, TTF_Font* font){
    this->x = x;
    this->y = y;
    this->width = width;
    this->height = height;
    this->font = font;
}

rectangleText::~rectangleText(){
    SDL_DestroyTexture(this->MessageTex);
    SDL_FreeSurface(this->surfaceMessage);
}

void rectangleText::setBackgroundColor(Uint8 r, Uint8 g, Uint8 b){
    this->backgroundColor = {r, g, b, 255};
}

void rectangleText::setTextColor(Uint8 r, Uint8 g, Uint8 b){
    this->textColor = {r, g, b, 255};
}

void rectangleText::draw(SDL_Renderer* renderer, int topSize){    
    
    SDL_DestroyTexture(this->MessageTex);
    SDL_FreeSurface(this->surfaceMessage);

    this->surfaceMessage = TTF_RenderUTF8_Blended_Wrapped(this->font, this->message.c_str(), this->textColor, this->width);  
    this->MessageTex = SDL_CreateTextureFromSurface(renderer, this->surfaceMessage);

    this->Message_rect.x = this->x; 
    this->Message_rect.y = topSize; 
    this->Message_rect.w = this->width; 
    this->Message_rect.h = this->surfaceMessage->h;


    SDL_RenderCopy(renderer, this->MessageTex, NULL, &this->Message_rect);
}

void rectangleText::setSize(int width, int height){
    this->width = width;
    this->height = height;
}
    
void rectangleText::setPosition(int x, int y){
    this->x = x;
    this->y = y;
}
void rectangleText::setMessage(std::string message){
    this->message = message;
}

void rectangleText::setFont(TTF_Font* font, unsigned int fontSize){
    this->fontSize = fontSize;
    TTF_CloseFont(this->font);
    this->font = NULL;
    this->font = font;
}