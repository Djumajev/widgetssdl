#pragma once
#include <string>
#include <SDL2/SDL.h>

struct color{
    Uint8 red, green, blue;
};

struct taskConstruct{
    std::string bigMessage,
        smallMessage,
        name;
    color textColor,
        topBackground,
        background;
};